# Overview for logout-manager
See the [full readme](src/usr/share/doc/logout-manager/README.md) farther down in the source tree.

## Alt directory
The `alt` directory contains alternative files, including a shell script implementation of logout-manager-trayicon, which depends on `mktrayicon`.
