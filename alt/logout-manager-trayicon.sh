#!/usr/bin/env sh
# File: logout-manager-trayicon.sh
# License: CC-BY-SA 4.0
# Author: bgstack15
# Startdate: 2020-03-19 13:34
# Title: Tray icon for logout-manager
# Purpose: To show a tray icon for logout options
# History:
#    Alternative version of a trayicon that depends on mktrayicon
# Usage:
# Reference:
#    keyboard-leds-trayicons
# Improve:
# Dependencies:
#    raw: mktrayicon, awk, xset
#    devuan: mktrayicon, mawk | gawk, x11-xserver-utils
# Documentation:
#    This script works just fine. I just want to learn how to do this in python, and have icons on the menu.

# CONFIG FILES
test -z "${LMT_GLOBAL_CONF}" && LMT_GLOBAL_CONF=/etc/logout-manager-trayicon.conf
test -z "${LMT_USER_CONF}" && LMT_USER_CONF="${HOME}/.config/logout-manager-trayicon.conf"
# also accept LMT_CONF

# FUNCTIONS

get_conf() {
   # Ripped from framework.sh
   # call: get_conf "${conffile}"
   local _infile="$1"
   local _tmpfile1="$( mktemp )"
   sed -e 's/^\s*//;s/\s*$//;/^[#$]/d;s/\s*[^\]#.*$//;' "${_infile}" | grep -viE "^$" | while read _line ;
   do
      local _left="$( echo "${_line}" | cut -d'=' -f1 )"
      eval "_thisval=\"\${${_left}}\""
      test -z "${_thisval}" && echo "${_line}" >> "${_tmpfile1}"
   done
   test -f "${_tmpfile1}" && { . "${_tmpfile1}" 1>/dev/null 2>&1 ; }
   /bin/rm -rf "${_tmpfile1}" 1>/dev/null 2>&1
}

clean_lmt() {
   { test -e "${lmicon}" && echo "q" > "${lmicon}" ; } 1>/dev/null 2>&1 &
}

# LOAD CONFIGS
# order is important! The last one called gets precedence.
# instead of simply dot-sourcing the conf file, pass it to get_conf which only applies new values, so this process's environment is preserved
for thisconf in "${LMT_GLOBAL_CONF}" "${LMT_USER_CONF}" "${LMT_CONF}" ;
do
   test -r "${thisconf}" && get_conf "${thisconf}"
done

# DEFAULTS in case configs did not have these values
test -z "${LMT_ICON}" && LMT_ICON=logout

# INITIALIZATION

lmicon="/var/run/user/$( id -u )/${$}.logout-manager.icon"

case "${1}" in
   "run-program")
      result="$( ps -eo 'pid,user,command:80' | grep -E 'logout-manager --from-[t]rayicon' | awk '{print $1}' )"
      if test -n "${result}"
      then
         kill "${result}"
      else
         /usr/bin/logout-manager --from-trayicon
      fi
      ;;
   *)
      test "ON" = "ON" && {
         mkfifo "${lmicon}"
         mktrayicon "${lmicon}" &
         echo "i ${LMT_ICON}" > "${lmicon}"
         echo "m lock,logout-manager-cli.py lock|logout,logout-manager-cli.py logout|hibernate,logout-manager-cli.py hibernate|shutdown,logout-manager-cli.py shutdown|reboot,logout-manager-cli.py reboot|-----|Logged in as ${USER}|hide tray icon,echo 'q' > ${lmicon} ; kill -10 $$ 1>/dev/null 2>&1" > "${lmicon}"
         echo "c $0 run-program" > "${lmicon}"
         echo "t ${DRYRUN:+DRYRUN MODE: }Logged in as $USER" > "${lmicon}"
      }
      ;;
esac

trap 'trap "" 2 10 ; clean_lmt'  2 10 # CTRL-C SIGUSR1
