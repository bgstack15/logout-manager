# README for logout-manager
## Introduction
Logout Manager is a python3 utility that provides a simple menu for logout-type actions. The supported actions are presented:
 * Lock
 * Logout
 * Hibernate (if supported by hardware)
 * Shutdown
 * Reboot

## Customization
The `lm-helper` logout command needs to be customized for every desktop environment. Some may need extra configurationon the window manager/desktop environment side.

### Fluxbox
For Fluxbox, you need to set a value in ~/.fluxbox/init

    session.screen0.allowRemoteActions:	true

Be aware that this is insecure. See man `fluxbox-remote(1)`.

## Alternatives
[oblogout](https://launchpad.net/oblogout) looks really old so I did not investigate personally, but it sounds like it does the same thing I am trying to do.
`apt-cache search logout` shows [lxsession-logout](http://manpages.ubuntu.com/manpages/precise/en/man1/lxsession-logout.1.html) which was compiled, as well as does not provide configurable options for changing executed commands or icons.

## License
[logout-manager-ncurses](src/usr/bin/logout-manager-ncurses) is licensed under the [MIT license](http://choosealicense.com/licenses/mit) and is derived almost entirely from [adamlamers](http://adamlamers.com/post/FTPD9KNRA8CT).
Everything else is licensed under [CC-BY-SA 4.0](https://choosealicense.com/licenses/cc-by-sa-4.0/).

## Description
This project is partially a programming playground for the [original author](https://bgstack15.wordpress.com) and partially a useful project for his migration to [Fluxbox](http://fluxbox.org/) on the desktop.

## Upsides
* This project is the first to [demonstrate SVG images in tkinter in python3](https://bgstack15.wordpress.com/2019/07/13/display-svg-in-tkinter-python3/) that I could find on the Internet.
* This project demonstrates how to have the Makefile and debian/rules build a dependency list, from the Dependencies tags of the files themselves.
* I have learned how to work with ncurses, gtk, and tcl in python3.
* This will make Fluxbox systems easier to use for general users.
* Does not use dbus!

## Downsides
* This whole thing is more complex than just logging out of my user session, and selecting a logout-type action from the display manager.
* Depends on sudo instead of using native tools.

## Changelog
### 0.0.3
* 2020-04-02
* Add man pages
* drop .py endings
* Adapt to python 3.8
  * fix [#2](https://gitlab.com/bgstack15/logout-manager/-/issues/2) AttributeError: module 'platform' has no attribute 'dist'
  * fix [#3](https://gitlab.com/bgstack15/logout-manager/-/issues/3) for Devuan Ceres 4: platform.platform() does not show "Devuan"
