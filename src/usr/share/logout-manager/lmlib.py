#!/usr/bin/env python3
# File: lmlib.py
# License: CC-BY-SA 4.0
# Author: bgstack15
# Startdate: 2019-06-12
# Title: Python libs for logout-manager
# Purpose: Store the common elements for operating a logout-manager
# History:
# Usage:
#    In a logout-manager-gtk program
# Reference:
#    platform info https://stackoverflow.com/questions/110362/how-can-i-find-the-current-os-in-python/10091465#10091465
# Improve:
# Documentation:
# Dependencies:
#    dep-devuan: python3-distro, python3:any

import configparser, os, subprocess
from distro import linux_distribution

logout_manager_version="2020-04-01a"

class Actions:

   def __take_action(command):
      print(command)
      command=str(command).split()
      command2=[]
      for i in command:
         command2.append(str(i.strip('"')))
      command=command2
      subprocess.run(command)

   @staticmethod
   def hibernate(config, event=None):
      #print("need to run the /sys/power/state trick, if available")
      Actions.__take_action(config.get_hibernate_command())

   @staticmethod
   def lock(config, event=None):
      #print("please lock the screen.")
      Actions.__take_action(config.get_lock_command())

   @staticmethod
   def logout(config, event=None):
      #print("please log out of current session!")
      Actions.__take_action(config.get_logout_command())

   @staticmethod
   def reboot(config, event=None):
      #print("please reboot.")
      Actions.__take_action(config.get_reboot_command())

   @staticmethod
   def shutdown(config, event=None):
      #print("please shut yourself down!")
      Actions.__take_action(config.get_shutdown_command())

class Config:
   def __init__(self):
      # load defaults which can be overwritten
      self.hibernate_command = ""
      self.lock_command = ""
      self.logout_command = ""
      self.reboot_command = ""
      self.shutdown_command = ""
      self.hibernate_icon = "system-hibernate"
      self.hibernate_fallback_icon = "system-hibernate"
      self.lock_icon = "system-lock-screen"
      self.lock_fallback_icon = "system-lock-screen"
      self.logout_icon = "system-log-out"
      self.logout_fallback_icon = "system-log-out"
      self.reboot_icon = "system-reboot"
      self.reboot_fallback_icon = "system-reboot"
      self.shutdown_icon = "system-shutdown"
      self.shutdown_fallback_icon = "system-shutdown"
      self.icon_size = 24
      self.icon_theme = "default"
      self.gtk3_default_icon_theme = "hicolor"
      self.icon_category = "actions"
      self.can_hibernate = False
      self.application_icon=("system-log-out")

   def set_hibernate_command(self,hibernate_command):
      self.hibernate_command = hibernate_command

   def set_lock_command(self,lock_command):
      self.lock_command = lock_command

   def set_logout_command(self,logout_command):
      self.logout_command = logout_command

   def set_reboot_command(self,reboot_command):
      self.reboot_command = reboot_command

   def set_shutdown_command(self,shutdown_command):
      self.shutdown_command = shutdown_command

   def set_hibernate_icon(self,hibernate_icon):
      self.hibernate_icon = hibernate_icon

   def set_lock_icon(self,lock_icon):
      self.lock_icon = lock_icon

   def set_logout_icon(self,logout_icon):
      self.logout_icon = logout_icon

   def set_reboot_icon(self,reboot_icon):
      self.reboot_icon = reboot_icon

   def set_shutdown_icon(self,shutdown_icon):
      self.shutdown_icon = shutdown_icon

   def set_icon_size(self,icon_size):
      self.icon_size = int(icon_size)

   def set_icon_theme(self,icon_theme):
      self.icon_theme = icon_theme

   def set_gtk3_default_icon_theme(self,icon_theme):
      self.gtk3_default_icon_theme= icon_theme

   def set_icon_category(self,icon_category):
      self.icon_category = icon_category

   def set_can_hibernate(self,can_hibernate):
      print("Setting can_hibernate:",can_hibernate)
      self.can_hibernate = bool(can_hibernate)

   def get_hibernate_command(self):
      return self.hibernate_command

   def get_lock_command(self):
      return self.lock_command

   def get_logout_command(self):
      return self.logout_command

   def get_reboot_command(self):
      return self.reboot_command

   def get_shutdown_command(self):
      return self.shutdown_command

   def get_hibernate_icon(self):
      return self.hibernate_icon

   def get_lock_icon(self):
      return self.lock_icon

   def get_logout_icon(self):
      return self.logout_icon

   def get_reboot_icon(self):
      return self.reboot_icon

   def get_shutdown_icon(self):
      return self.shutdown_icon

   def get_hibernate_fallback_icon(self):
      return self.hibernate_fallback_icon

   def get_lock_fallback_icon(self):
      return self.lock_fallback_icon

   def get_logout_fallback_icon(self):
      return self.logout_fallback_icon

   def get_reboot_fallback_icon(self):
      return self.reboot_fallback_icon

   def get_shutdown_fallback_icon(self):
      return self.shutdown_fallback_icon

   def get_icon_size(self):
      return self.icon_size

   def get_icon_theme(self):
      return self.icon_theme

   def get_gtk3_default_icon_theme(self):
      return self.gtk3_default_icon_theme

   def get_icon_category(self):
      return self.icon_category

   def get_can_hibernate(self):
      return self.can_hibernate

def get_gtk3_default_icon_theme():
   # abstracted so it does not clutter get_scaled_icon
   name = "hicolor"
   gtk3_config_path = os.path.join(os.path.expanduser("~"),".config","gtk-3.0","settings.ini")
   gtk3_config = configparser.ConfigParser()
   gtk3_config.read(gtk3_config_path)
   try:
      if 'Settings' in gtk3_config:
         name = gtk3_config['Settings']['gtk-icon-theme-name']
      elif 'settings' in gtk3_config:
         name = gtk3_config['settings']['gtk-icon-theme-name']
   except:
      # supposed failsafe: keep name = hicolor
      pass
   print("Found gtk3 default theme:",name)
   return name

def Initialize_config(infile):
   # Read config
   config_in = configparser.ConfigParser()
   config_in.read(infile)
   config = Config()
   try:
      ci = config_in['logout-manager']
   except:
      # no definition
      print("Using default commands")

   try:
      ci_icons = config_in['icons']
   except:
      # no definition
      print("Using default icons")

   # load up our custom class, which stores the defaults in case we do not set them here
   if 'hibernate_command' in ci:
      config.set_hibernate_command(ci['hibernate_command'])
   if 'lock_command' in ci:
      config.set_lock_command(ci['lock_command'])
   if 'logout_command' in ci:
      config.set_logout_command(ci['logout_command'])
   if 'reboot_command' in ci:
      config.set_reboot_command(ci['reboot_command'])
   if 'shutdown_command' in ci:
      config.set_shutdown_command(ci['shutdown_command'])
   if 'hibernate' in ci_icons:
      config.set_hibernate_icon(ci_icons['hibernate'])
   if 'lock' in ci_icons:
      config.set_lock_icon(ci_icons['lock'])
   if 'logout' in ci_icons:
      config.set_logout_icon(ci_icons['logout'])
   if 'reboot' in ci_icons:
      config.set_reboot_icon(ci_icons['reboot'])
   if 'shutdown' in ci_icons:
      config.set_shutdown_icon(ci_icons['shutdown'])
   if 'size' in ci_icons:
      config.set_icon_size(ci_icons['size'])
   if 'theme' in ci_icons:
      config.set_icon_theme(ci_icons['theme'])
   # store the info about if hibernate is an option
   can_hibernate = False
   try:
      with open('/sys/power/state') as r:
         line = r.read()
         if 'disk' in line: can_hibernate = True
   except:
      pass
   config.set_can_hibernate(can_hibernate)

   # read gtk3_default_icon_theme
   config.set_gtk3_default_icon_theme(get_gtk3_default_icon_theme())
   if config.get_icon_theme() == "default":
      config.set_icon_theme(config.get_gtk3_default_icon_theme())

   # set icon category
   # written primarily for el7 which uses "app" for the system-reboot icons, etc.
   a, b, _ = linux_distribution()
   a = a.lower()
   try:
      if ("red hat" in a or "redhat" in a) and int(b.split(".")[0]) <= 7:
         config.set_icon_category("apps")
   except:
      pass

   # DEBUG, raw from conf file and system status
   print("Raw values:")
   for item in config_in.sections():
      print("["+item+"]")
      for key in config_in[item]:
         print(key+" = "+config_in[item][key])
   print("Can hibernate:",can_hibernate)

   # DEBUG, stored values
   print("Stored values:")
   print(config.get_hibernate_command())
   print(config.get_lock_command())
   print(config.get_logout_command())
   print(config.get_reboot_command())
   print(config.get_shutdown_command())
   print(config.get_hibernate_icon())
   print(config.get_lock_icon())
   print(config.get_logout_icon())
   print(config.get_reboot_icon())
   print(config.get_shutdown_icon())
   print(config.get_icon_size())
   print(config.get_icon_theme())
   print(config.get_icon_category())
   print("Can hibernate:",config.get_can_hibernate())

   return config
