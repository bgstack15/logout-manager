logout-manager-tcl 1 "April 2020" logout-manager "General Commands Manual"
==================================================
# NAME
logout-manager-tcl - Tcl/tk interface for invoking logout options
# SYNOPSIS
logout-manager-tcl
# DESCRIPTION
Use this interface to `logout-manager`(7) in an X11 graphical environment. Using the python3 tkinter library, this program displays buttons with text and icons to present a simple menu for invoking different logout-related commands.
# ACTIONS
When a button is selected, an action is invoked. See "ACTIONS" section of `logout-manager`(7) man page for a list of available actions.
# AUTHOR
bgstack15 `https://bgstack15.wordpress.com/`
# COPYRIGHT
CC-BY-SA 4.0
# SEE ALSO
`logout-manager`(7),`logout-manager.conf`(5)
