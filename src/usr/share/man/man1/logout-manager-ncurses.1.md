logout-manager-ncurses 1 "April 2020" logout-manager "General Commands Manual"
==================================================
# NAME
logout-manager-ncurses - ncurses interface for invoking logout options
# SYNOPSIS
logout-manager-ncurses
# DESCRIPTION
Use this interface to `logout-manager`(7) in a console or terminal window. It displays numerized options, which can also be navigated with arrow keys or vim-style [jk] keys. The presented menu shows a list of actions for invoking different logout-related commands.
# ACTIONS
When an entry is selected, an action is invoked. See "ACTIONS" section of `logout-manager`(7) man page for a list of available actions.
# AUTHOR
bgstack15 `https://bgstack15.wordpress.com/`
# COPYRIGHT
CC-BY-SA 4.0
# SEE ALSO
`logout-manager`(7),`logout-manager.conf`(5)
