logout-manager-cli 1 "April 2020" logout-manager "General Commands Manual"
==================================================
# NAME
logout-manager-cli - command line interface for invoking logout options
# SYNOPSIS
logout-manager-cli [OPTIONS] [ACTION]
# DESCRIPTION
Use this interface to `logout-manager`(7) from the command line or in scripts. Parameters can be passed to determine the action to take.
# OPTIONS

-h, --help     show a help screen  

-d [0-10]      set debug level  

-n             Dryrun only! Do not take action. Useful with `hibernate` to determine if hibernate is allowed on this system.  

-V  --version  Display version and exit
# ACTIONS
One action may be specified on the command line.  See "ACTIONS" section of `logout-manager`(7) man page for a list of available actions.
# AUTHOR
bgstack15 `https://bgstack15.wordpress.com/`
# COPYRIGHT
CC-BY-SA 4.0
# SEE ALSO
`logout-manager`(7),`logout-manager.conf`(5)
