logout-manager-trayicon 1 "April 2020" logout-manager "General Commands Manual"
==================================================
# NAME
logout-manager-trayicon - system tray icon for invoking logout options
# SYNOPSIS
logout-manager-trayicon
# DESCRIPTION
This interface to `logout-manager`(7) displays a system tray (notification area) icon. `Right-clicking` the icon displays a popup menu with options for invoking different logout-related commands.
A disabled menu entry also shows the currently-logged in user, as well as a notification if `DRYRUN` is set.
`Double-clicking` the icon will invoke `logout-manager` which is usually aliases to one of the available interfaces for `logout-manager`(7)
# ACTIONS
When a button from the menu is selected, an action is invoked. See "ACTIONS" section of `logout-manager`(7) man page for a list of available actions.
# AUTHOR
bgstack15 `https://bgstack15.wordpress.com/`
# COPYRIGHT
CC-BY-SA 4.0
# SEE ALSO
`logout-manager`(7),`logout-manager.conf`(5)
