logout-manager 7 "April 2020" logout-manager "Common configuration options"
==================================================
# NAME
logout-manager - common configuration options
# SYNOPSIS
This program manages a list of options for common logout commands, and can be used by a number of front-ends, including:

`logout-manager-cli`(1)  
`logout-manager-gtk`(1)  
`logout-manager-ncurses`(1)  
`logout-manager-tcl`(1)  
`logout-manager-trayicon`(1)

Of the listed front-ends, the trayicon and cli are not recommended for aliasing to the short name `logout-manager`.

The library for logout-manager uses a number of environment variables; see below.
# ENVIRONMENT VARIABLES
If the defaults file (nominally `/etc/sysconfig/logout-manager`) does not define the following variables, you can define them in your own situation.

`LOGOUT_MANAGER_LIBPATH`=/usr/share/logout-manager

`LOGOUT_MANAGER_CONF`=/etc/logout-manager.conf

Additional environment variables are used.
`DRYRUN`=1  If DRYRUN is set to any non-null value, the actions will instead only display what would be executed and will not execute the commands.

# ACTIONS
An action is one of the following.

**lock** hide and password-protect current screen

**logout** close this graphical session

**hibernate** save session memory to disk and power off. Note that not all hardware supports this.

**shutdown** power off

**reboot** restarts the system

# AUTHOR
bgstack15 `https://bgstack15.wordpress.com/`
# COPYRIGHT
CC-BY-SA 4.0
# SEE ALSO
`logout-manager.conf`(5), `logout-manager-cli`(1), `logout-manager-gtk`(1), `logout-manager-ncurses`(1), `logout-manager-tcl`(1), `logout-manager-trayicon`(1)
