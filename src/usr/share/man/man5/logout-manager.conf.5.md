logout-manager.conf 5 "April 2020" logout-manager "File Formats and Conventions"
==================================================
# NAME
logout-manager.conf - the configuration file for logout-manager
# FILE FORMAT
The file has an ini-style syntax and consists of sections and parameters. A section begins with the name of the section in square brackets and continues until the next section begins. An example:

    [section]
    key = "value"
    key2 = value2

Put a value in double quotes if you need the white space preserved.  
logout-manager.conf must be a regular file, and readable by all users who are permitted to run logout-manager. This is usually the same users who are permitted to log in to a graphical session.
# SECTIONS AND EXAMPLES
### The [logout-manager] section
Define the command for a given action.

`Section parameters`

lock_command, logout_command, hibernate_command, shutdown_command, reboot_command
    The value will be invoked upon selection of the named action. Place a command with its parameters in double quotes.
    Defaults:

    lock_command="/usr/libexec/logout-manager/lm-helper lock"
    logout_command="/usr/libexec/logout-manager/lm-helper logout"
    hibernate_command="sudo /usr/libexec/logout-manager/lm-helper hibernate"
    reboot_command="sudo /usr/libexec/logout-manager/lm-helper reboot"
    shutdown_command="sudo /usr/libexec/logout-manager/lm-helper shutdown"
### The [icons] section
Configuration options for which icons to display, for the gtk3, tcl, and trayicon front-ends.

`Section parameters`

size (integer)
    The height and width, in pixels, of the icons for the gtk3 and tcl frontends.
    Default: 24

theme
    The gtk3 icon theme. Options include your currently installed themes in `/usr/share/icons`, or the string *default* which loads the current gtk3 theme as defined in `${HOME}/.config/gtk-3.0/settings.ini`, value `gtk-icon-theme-name`.
    Default: default

lock, logout, hibernate, shutdown, reboot
    Each of these entries can be given an icon name, e.g., "system-reboot", or a full path to a specific file.
    Defaults:

    lock = system-lock-screen
    logout = system-log-out
    hibernate = system-hibernate
    shutdown = system-shutdown
    reboot = system-reboot
# AUTHOR
bgstack15 `https://bgstack15.wordpress.com/`
# COPYRIGHT
CC-BY-SA 4.0
# SEE ALSO
`logout-manager`(7)  
Icon theme specification `http://www.freedesktop.org/wiki/Specifications/icon-theme-spec/`
